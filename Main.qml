import QtQuick

Rectangle {

    id: theRect

    readonly property Rectangle aRect: Rectangle {
        width: 10
        height: 10

        // color: theRect.color
    }

    property var theColor: aRect.color

}
